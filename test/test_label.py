from display.main import GUI

import sys
from PyQt5.QtWidgets import QApplication

app = QApplication(sys.argv + ['-platform', 'minimal'])


def test_label():
    window = GUI()
    window.show()
    assert window.label.text() =='lab'


def test_label2():
    window = GUI()
    window.show()
    assert window.label.text() !='lab1'
