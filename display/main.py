from PyQt5.QtWidgets import QMainWindow, QLabel


class GUI(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Memory App')
        self.statusBar().showMessage('Status bar') # todo This will need to get updated from database, or something

        self.resize(800, 400)
        self.label = QLabel('lab', self)
